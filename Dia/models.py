from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager

# Create your models here.

class ManagerUser(BaseUserManager):
    def create_user(self, matricula, first_name, cpf, email, password=None):
        user = self.model(
            matricula = matricula,
            email = email,
            first_name = first_name,
            cpf = cpf,
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, matricula, first_name, cpf, email, password):
        user = self.create_user(
            matricula=matricula,
            email=email,
            first_name=first_name,
            cpf=cpf,
            password= password,
        )
        user.administrador = True
        user.staff = True
        user.moderador = True
        user.save(using=self._db)
        return user

class User(AbstractBaseUser):

    cpf = models.CharField(max_length=14, primary_key=True, unique=True, verbose_name='CPF')
    matricula = models.CharField(max_length=10, unique=True, null=False, verbose_name='Matricula')
    email = models.EmailField(max_length=30, unique=True, verbose_name='Email')
    administrador = models.BooleanField(default=False)
    moderador = models.BooleanField(default=False)
    staff = models.BooleanField(default=False)
    ativo = models.BooleanField(default=True)

    USERNAME_FIELD = 'matricula'
    REQUIRED_FIELDS = ('first_name', 'cpf', 'email')

    objects = ManagerUser()

    def __str__(self):
        return self.first_name

