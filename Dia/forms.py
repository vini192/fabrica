from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.forms import UserCreationForm
from django.forms import ModelForm
from Dia.models import User


class RegistrarForm(UserCreationForm):
    matricula = forms.CharField(max_length=10, help_text='*Coloque uma Matrícula Válida.')

    class Meta:
        model = User
        fields = ('matricula', 'first_name', 'cpf', 'email', 'password1', 'password2')

class AutenticacaoForm(forms.ModelForm):
    password = forms.CharField(label='Password', widget=forms.PasswordInput)

    class Meta:
        model =  User
        fields = ('matricula', 'password')

    def limpa(self):
        if self.is_valid():
            matricula = self.cleaned_date['matricula']
            password = self.cleaned_data['password']
            if not authenticate(matricula=matricula, password=password):
                raise forms.ValidationError('Dados Inválidos!')

class UpdateForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('matricula', 'password')
